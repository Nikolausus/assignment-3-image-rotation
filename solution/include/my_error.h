#ifndef MY_ERROR
#define MY_ERROR

#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_FILENAME,
    READ_INVALID_HEADER,
    READ_INVALID_BITS
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BITS,
    WRITE_ERROR,
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

void exit_error(const char* err);

void call_read_error(enum read_status readStatus);

void call_write_error(enum write_status writeStatus);

void call_close_error(enum close_status closeStatus);

#endif
