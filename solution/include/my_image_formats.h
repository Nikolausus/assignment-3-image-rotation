#ifndef MY_IMAGE_FORMATS
#define MY_IMAGE_FORMATS

#include <inttypes.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct pixel_line {
    struct pixel* pixels;
};

struct image {
    struct pixel_line* pixel_lines;
    size_t width, height;
};

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

void destroy_pixel_lines(struct pixel_line *old, size_t height);

void destroy_image(struct image *old);

struct pixel_line* generate_new_empty_pixel_line(size_t width, size_t height);

struct bmp_header generate_header(struct image *image);

#endif
