#ifndef ROTATE_IMAGE
#define ROTATE_IMAGE

#include "my_image_formats.h"

void rotate_image(struct image *image, const int64_t degree);

#endif
