#ifndef UTIL
#define UTIL

#include "my_error.h"

#include <inttypes.h>
#include <stdlib.h>

int64_t parse_angle_of_rotation(const char* str);

void parse_arguments(char* *input_filename, char* *output_filename, int64_t *angle_of_rotation, int argc, char** argv);

#endif
