#ifndef WORK_WITH_BMP_FILE
#define WORK_WITH_BMP_FILE

#include "my_error.h"
#include "my_image_formats.h"

#include <stdbool.h>

enum read_status open_bmp_file_read(const char* fname, FILE** input_img);

enum read_status read_bmp_file(FILE** input_img, struct bmp_header *header, struct image *image);

enum close_status close_bmp_file_read(FILE** input_img);

enum write_status open_bmp_file_write(const char* fname, FILE** output_img);

enum write_status write_to_bmp_file(FILE** output_img, struct image *image);

enum close_status close_bmp_file_write(FILE** output_img);

#endif
