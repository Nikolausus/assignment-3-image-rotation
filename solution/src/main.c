#include "my_error.h"
#include "my_image_formats.h"
#include "rotate_image.h"
#include "util.h"
#include "work_with_bmp_file.h"

/*
void test(void ) {
    struct bmp_header header = {0};
    struct image image = {0};

    enum read_status readStatus;
    FILE *input_img = NULL;

    enum write_status writeStatus;
    FILE *output_img = NULL;

    enum close_status closeStatus;

    const char *input_filename = "C:\\Users\\Inherency\\CLionProjects\\assignment-3-image-rotation\\solution\\test.bmp";
    const char *output_filename = "C:\\Users\\Inherency\\CLionProjects\\assignment-3-image-rotation\\solution\\output.bmp";

    readStatus = open_bmp_file_read(input_filename, &input_img);
    if (readStatus != READ_OK) {
        close_bmp_file_read(&input_img);
        call_read_error(readStatus);
    }

    readStatus = read_bmp_file(&input_img, &header, &image);
    if (readStatus != READ_OK) {
        close_bmp_file_read(&input_img);
        call_read_error(readStatus);
    }

    closeStatus = close_bmp_file_read(&input_img);
    if (closeStatus != CLOSE_OK) {
        call_close_error(closeStatus);
    }

    rotate_image(&image, 0);

    writeStatus = open_bmp_file_write(output_filename, &output_img);
    if (writeStatus != WRITE_OK) {
        close_bmp_file_write(&output_img);
        call_write_error(writeStatus);
    }

    writeStatus = write_to_bmp_file(&output_img, &image);
    if (writeStatus != WRITE_OK) {
        close_bmp_file_write(&output_img);
        call_write_error(writeStatus);
    }

    closeStatus = close_bmp_file_write(&output_img);
    if (closeStatus != CLOSE_OK) {
        call_close_error(closeStatus);
    }
    destroy_pixel_lines(image.pixel_lines, image.height);
}*/

int main( int argc, char** argv ) {

    // Распаковываем аргументы
    char* input_filename;
    char* output_filename;
    int64_t angle_of_rotation;
    parse_arguments(&input_filename, &output_filename, &angle_of_rotation, argc, argv);

    // Инициализируем структуры
    struct bmp_header header = {0};
    struct image image = {0};

    // Статусы ошибок
    enum read_status readStatus;
    enum close_status closeStatus;
    enum write_status writeStatus;

    // Дескрипторы рабочих файлов
    FILE *input_img = NULL;
    FILE *output_img = NULL;

    readStatus = open_bmp_file_read(input_filename, &input_img);
    if (readStatus != READ_OK) {
        close_bmp_file_read(&input_img);
        call_read_error(readStatus);
    }

    readStatus = read_bmp_file(&input_img, &header, &image);
    if (readStatus != READ_OK) {
        close_bmp_file_read(&input_img);
        call_read_error(readStatus);
    }

    closeStatus = close_bmp_file_read(&input_img);
    if (closeStatus != CLOSE_OK) {
        call_close_error(closeStatus);
    }

    rotate_image(&image, angle_of_rotation);

    writeStatus = open_bmp_file_write(output_filename, &output_img);
    if (writeStatus != WRITE_OK) {
        close_bmp_file_write(&output_img);
        call_write_error(writeStatus);
    }

    writeStatus = write_to_bmp_file(&output_img, &image);
    if (writeStatus != WRITE_OK) {
        close_bmp_file_write(&output_img);
        call_write_error(writeStatus);
    }

    closeStatus = close_bmp_file_write(&output_img);
    if (closeStatus != CLOSE_OK) {
        call_close_error(closeStatus);
    }
    destroy_pixel_lines(image.pixel_lines, image.height);
    return 0;

//    test();
}
