#include "my_error.h"
#include <stdlib.h>

const char* string_read_error[] = {
        [READ_INVALID_FILENAME] = "Invalid filename\n",
        [READ_INVALID_HEADER] = "Invalid bmp header\n",
        [READ_INVALID_BITS] = "Invalid bits sequence\n"
};

const char* string_write_error[] = {
        [WRITE_INVALID_HEADER] = "Invalid write header\n",
        [WRITE_INVALID_BITS] = "Invalid bits sequence\n",
        [WRITE_ERROR] = "Unexpected write error\n"
};

const char* string_close_error[] = {
        [CLOSE_ERROR] = "Unexpected close error\n"
};

void exit_error(const char* err) {
    printf("%s", err);
    exit(1);
}

void call_read_error(enum read_status readStatus) {
    if (string_read_error[readStatus] != NULL) {
        exit_error(string_read_error[readStatus]);
    } else {
        exit_error("Unexpected read error\n");
    }
}

void call_write_error(enum write_status writeStatus) {
    if (string_write_error[writeStatus] != NULL) {
        exit_error(string_write_error[writeStatus]);
    } else {
        exit_error("Unexpected write error\n");
    }
}

void call_close_error(enum close_status closeStatus) {
    if (string_close_error[closeStatus] != NULL) {
        exit_error(string_close_error[closeStatus]);
    } else {
        exit_error("Unexpected close error\n");
    }
}
