#include "my_image_formats.h"

void destroy_pixel_lines(struct pixel_line *old, size_t height) {
    for (size_t y = 0; y < height; ++y) {
        free(old[y].pixels);
    }
    free(old);
}

void destroy_image(struct image *old) {
    destroy_pixel_lines(old->pixel_lines, old->height);
    free(old);
}

struct pixel_line* generate_new_empty_pixel_line(size_t width, size_t height) {
    struct pixel_line* new = calloc(height, sizeof(struct pixel_line));
    for (size_t y = 0; y < height; ++y) {
        new[y].pixels = calloc(width, sizeof(struct pixel));
    }
    return new;
}

struct bmp_header generate_header(struct image *image) {
    return (struct bmp_header) {
            .bfType = 19778,
            .bfileSize = image->width * image->height * 3 + ((4 - image->width * 3 % 4) % 4) * image->height + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = sizeof(struct bmp_header) - sizeof(uint32_t) * 3 - sizeof(uint16_t),
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = image->width * image->height * 3 + ((4 - image->width * 3 % 4) % 4) * image->height,
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}
