#include "rotate_image.h"

struct pixel_line* rotate_on_90(struct image *old) {
    size_t width = old->height, height = old->width;
    struct pixel_line *new = generate_new_empty_pixel_line(width, height);
    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; ++x) {
            new[y].pixels[x] = old->pixel_lines[x].pixels[height - y - 1];
        }
    }
    destroy_pixel_lines(old->pixel_lines, old->height);
    return new;
}

struct pixel_line* rotate_on_180(struct image *old) {
    size_t width = old->width, height = old->height;
    struct pixel_line *new = generate_new_empty_pixel_line(width, height);
    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; ++x) {
            new[y].pixels[x] = old->pixel_lines[height - y - 1].pixels[width - x - 1];
        }
    }
    destroy_pixel_lines(old->pixel_lines, old->height);
    return new;
}

struct pixel_line* rotate_on_270(struct image *old) {
    size_t width = old->height, height = old->width;
    struct pixel_line *new = generate_new_empty_pixel_line(width, height);
    for (size_t y = 0; y < height; ++y) {
        for (size_t x = 0; x < width; ++x) {
            new[y].pixels[x] = old->pixel_lines[width - x - 1].pixels[y];
        }
    }
    destroy_pixel_lines(old->pixel_lines, old->height);
    return new;
}

void rotate_image(struct image *image, const int64_t degree) {
    switch (degree % 360) {
        case 0: {
//            printf("0\n");
            break;
        }
        case 90:
        case -270: {
//            printf("90 or -270\n");
            image->pixel_lines = rotate_on_90(image);
            image->width ^= image->height;
            image->height ^= image->width;
            image->width ^= image->height;
            break;
        }
        case 180:
        case -180: {
//            printf("180 or -180\n");
            image->pixel_lines = rotate_on_180(image);
            break;
        }
        case 270:
        case -90: {
//            printf("270 or -90");
            image->pixel_lines = rotate_on_270(image);
            image->width ^= image->height;
            image->height ^= image->width;
            image->width ^= image->height;
            break;
        }
        default: {
            break;
        }
    }
}
