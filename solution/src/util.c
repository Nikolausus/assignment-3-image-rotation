#include "util.h"

int64_t parse_angle_of_rotation(const char* str) {
    int64_t angle_of_rotation = atoi(str);
    if (angle_of_rotation == 0 && str[0] != '0') {
        exit_error("<angle_of_rotation> must be integer");
    } else if (angle_of_rotation % 90 != 0) {
        exit_error("<angle_of_rotation> must be a multiple of 90");
    }
    return angle_of_rotation;
}

void parse_arguments(char* *input_filename, char* *output_filename, int64_t *angle_of_rotation, int argc, char** argv) {
    if (argc != 4) {
        exit_error("The input format should be as follows: <input_filename> <output_filename> <angle_of_rotation>\n");
    }

    *input_filename = argv[1];
    *output_filename = argv[2];
    *angle_of_rotation = parse_angle_of_rotation(argv[3]);
}
