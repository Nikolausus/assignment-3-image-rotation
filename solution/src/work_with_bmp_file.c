#include "work_with_bmp_file.h"

bool read_header( FILE* img, struct bmp_header* header) {
    return fread(header, sizeof(struct bmp_header), 1, img);
}

bool read_pixel_line(FILE* img, struct pixel_line* pixelLine, size_t length) {
    return fread(pixelLine->pixels, sizeof(struct pixel), length, img);
}

size_t read_trash_bytes(FILE* img, struct pixel *trash, size_t count_trash_bytes) {
    return fread(trash, count_trash_bytes, 1, img);
}

bool read_pixels(FILE *img, struct bmp_header* header, struct image *image) {
    image->height = header->biHeight;
    image->width = header->biWidth;
    image->pixel_lines = generate_new_empty_pixel_line(image->width, image->height);
    struct pixel trash_buff = {255, 76, 91};
    for (size_t y = 0; y < image->height; ++y) {
        if (!read_pixel_line(img, &image->pixel_lines[y], image->width)) {
//            printf("Line %zu\nCan't read pixel line\n", y);
            destroy_image(image);
            return false;
        }
        if (image->width * 3 % 4 != 0) {
            read_trash_bytes(img, &trash_buff, 4 - image->width * 3 % 4);
//            printf("Read %zu bytes\n", 4 - image->width * 3 % 4);
        }
    }
    return true;
}

enum read_status open_bmp_file_read(const char* fname, FILE** input_img) {
    *input_img = fopen(fname, "rb");
    if (input_img == NULL) {
        return READ_INVALID_FILENAME;
    }
    return READ_OK;
}

enum read_status read_bmp_file(FILE** input_img, struct bmp_header *header, struct image *image) {
    if (!read_header(*input_img, header)) {
        return READ_INVALID_HEADER;
    }
    if (!read_pixels(*input_img, header, image)) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum close_status close_bmp_file_read(FILE** input_img) {
    if (input_img != NULL) {
        fclose(*input_img);
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
}

bool write_pixel_line(FILE* img, struct pixel_line* pixelLine, size_t length) {
    return fwrite(pixelLine->pixels, sizeof(struct pixel), length, img);
}

size_t write_trash_bytes(FILE* img, struct pixel *trash_buff, size_t count_trash_bytes) {
    return fwrite(trash_buff, count_trash_bytes, 1, img);
}

bool write_header(FILE *output_img, struct bmp_header *header) {
    return fwrite(header, sizeof(struct  bmp_header), 1, output_img);
}

bool write_pixels(FILE *img, struct image *image) {
    struct pixel trash_buff = {255, 76, 91};
    for (size_t y = 0; y < image->height; ++y) {
        if (!write_pixel_line(img, &image->pixel_lines[y], image->width)) {
            destroy_image(image);
            return false;
//            printf("Can't write pixel line %zu\n", y);
        }
        if (image->width * 3 % 4 != 0) {
            write_trash_bytes(img, &trash_buff, 4 - image->width * 3 % 4);
//            printf("Write %zu bytes\n", 4 - image->width * 3 % 4);
        }
    }
    return true;
}

enum write_status open_bmp_file_write(const char* fname, FILE** output_img) {
    *output_img = fopen(fname, "wb");
    if (output_img == NULL) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_to_bmp_file(FILE** output_img, struct image *image) {
    struct bmp_header header = generate_header(image);
    if (!write_header(*output_img, &header)) {
        return WRITE_INVALID_HEADER;
    }
    if (!write_pixels(*output_img, image)) {
        return WRITE_INVALID_BITS;
    }
    return WRITE_OK;
}

enum close_status close_bmp_file_write(FILE** output_img) {
    if (output_img != NULL) {
        fclose(*output_img);
        return CLOSE_OK;
    }
    return CLOSE_ERROR;
}
